/**
 * file: PumpOutputCalc.java
 * 
 * author: Timothy Carter
 * 
 * date: October 18, 2009
 * 
 **/
package com.PumpOutputCalc;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class PumpOutputCalc extends JPanel implements ActionListener {
	private static final long serialVersionUID = 1L;
	static String[] OZLab = { "oz/stroke", "ml/stroke" };
	static String[] OZPer = { "oz", "ml" };
	static Color Blue = new Color(0, 0, 250);
	static Color LtBlue = new Color(0, 80, 180);
	static Color White = new Color(250, 250, 250);
	static Color Yellow = new Color(190, 210, 0);
	static Color Red = new Color(250, 0, 0);
	static Color LtRed = new Color(180, 0, 0);
	static Color Black = new Color(0, 0, 0);
	static JLabel PumpChoice = new JLabel();
	static JTextField OZperPulse = new JTextField();
	static JComboBox OZLabel = new JComboBox(OZLab);
	static JLabel StrokeRate = new JLabel();
	static JLabel MaxStrokeRate = new JLabel();
	static JTextField MaxStroke = new JTextField();
	static JTextField AmtPerDay = new JTextField();
	static JLabel AmtPumped = new JLabel();
	static JComboBox AmtPer = new JComboBox(OZPer);
	static JLabel SetPumpTo = new JLabel();
	static JTextField PumpStrokes = new JTextField();
	static JLabel PerHour = new JLabel();
	static JLabel AlsoPumpTo = new JLabel();
	static JTextField AlsoStrokes = new JTextField();
	static JLabel PerMinute = new JLabel();
	static JLabel Notice = new JLabel();
	static JLabel Notice2 = new JLabel();
	
	public PumpOutputCalc() {
		// Create Radio Buttons
		JRadioButton B130 = new JRadioButton("130");
		B130.setOpaque(false);
		B130.setForeground(White);
		B130.addActionListener(this);
		B130.setActionCommand("B130Enabled");
		JRadioButton B217 = new JRadioButton("217");
		B217.setOpaque(false);
		B217.setForeground(White);
		B217.addActionListener(this);
		B217.setActionCommand("B217Enabled");
		JRadioButton B115 = new JRadioButton("115");
		B115.setOpaque(false);
		B115.setForeground(White);
		B115.addActionListener(this);
		B115.setActionCommand("B115Enabled");
		JRadioButton B155 = new JRadioButton("155");
		B155.setOpaque(false);
		B155.setForeground(White);
		B155.addActionListener(this);
		B155.setActionCommand("B155Enabled");
		JRadioButton Other = new JRadioButton("Other");
		Other.setOpaque(false);
		Other.setForeground(White);
		Other.addActionListener(this);
		Other.setActionCommand("OtherEnabled");
		// Setup buttons in group
		ButtonGroup PumpSelect = new ButtonGroup();
		PumpSelect.add(B130);
		PumpSelect.add(B217);
		PumpSelect.add(B115);
		PumpSelect.add(B155);
		PumpSelect.add(Other);
		
		JPanel PumpChoicePanel = new JPanel();
		PumpChoicePanel.setLayout(new BoxLayout(PumpChoicePanel, BoxLayout.X_AXIS));
		PumpChoice.setForeground(White);
		PumpChoice.setText("Select the type of pump: ");
		PumpChoicePanel.setOpaque(false);
		PumpChoicePanel.add(PumpChoice);
		PumpChoicePanel.add(B130);
		PumpChoicePanel.add(B217);
		PumpChoicePanel.add(B115);
		PumpChoicePanel.add(B155);
		PumpChoicePanel.add(Other);
		OZperPulse.setColumns(5);
		OZperPulse.setText("0");
		OZperPulse.setVisible(true);
		OZperPulse.addActionListener(this);
		OZperPulse.setActionCommand("Recalculate");
		OZLabel.setSelectedIndex(0);
		OZLabel.setForeground(Black);
		OZLabel.addActionListener(this);
		OZLabel.setActionCommand("Recalculate");
		OZLabel.setOpaque(false);
		PumpChoicePanel.add(OZperPulse);
		PumpChoicePanel.add(OZLabel);

		JPanel StrokePanel = new JPanel();
		StrokePanel.setLayout(new BoxLayout(StrokePanel, BoxLayout.X_AXIS));
		StrokePanel.setOpaque(false);
		MaxStrokeRate.setText("Maximum Stroke Rate:");
		MaxStrokeRate.setForeground(White);
		MaxStroke.setColumns(5);
		MaxStroke.setText("0");
		StrokeRate.setText("Strokes / Min");
		StrokeRate.setForeground(White);
		StrokePanel.add(MaxStrokeRate);
		StrokePanel.add(MaxStroke);
		StrokePanel.add(StrokeRate);
		
		JPanel AmtDayPanel = new JPanel();
		AmtDayPanel.setLayout(new BoxLayout(AmtDayPanel, BoxLayout.X_AXIS));
		AmtDayPanel.setOpaque(false);
		AmtPumped.setText("Amount to be pumped per day:");
		AmtPumped.setForeground(White);
		AmtPerDay.setColumns(10);
		AmtPerDay.addActionListener(this);
		AmtPerDay.setActionCommand("Recalculate");
		AmtPerDay.setText("0");
		AmtPer.addActionListener(this);
		AmtPer.setActionCommand("Recalculate");
		AmtPer.setOpaque(false);
		AmtDayPanel.add(AmtPumped);
		AmtDayPanel.add(AmtPerDay);
		AmtDayPanel.add(AmtPer);
		
		JPanel SetPumpPanel = new JPanel();
		SetPumpPanel.setLayout(new BoxLayout(SetPumpPanel, BoxLayout.X_AXIS));
		SetPumpPanel.setOpaque(false);
		SetPumpTo.setText("You may set your pump to: ");
		SetPumpTo.setForeground(White);
		PumpStrokes.setEnabled(false);
		PumpStrokes.setDisabledTextColor(Blue);
		PumpStrokes.setColumns(10);
		PumpStrokes.setText(MaxStroke.getText());
		PerHour.setText("Strokes/HR");
		PerHour.setForeground(White);
		SetPumpPanel.add(SetPumpTo);
		SetPumpPanel.add(PumpStrokes);
		SetPumpPanel.add(PerHour);
		
		JPanel SetPumpPanel2 = new JPanel();
		SetPumpPanel2.setLayout(new BoxLayout(SetPumpPanel2, BoxLayout.X_AXIS));
		SetPumpPanel2.setOpaque(false);
		AlsoPumpTo.setText("You may also set your pump to: ");
		AlsoPumpTo.setForeground(White);
		AlsoStrokes.setColumns(10);
		AlsoStrokes.setText(MaxStroke.getText());
		AlsoStrokes.setEnabled(false);
		AlsoStrokes.setDisabledTextColor(Blue);
		PerMinute.setText("Strokes/MIN");
		PerMinute.setForeground(White);
		SetPumpPanel2.add(AlsoPumpTo);
		SetPumpPanel2.add(AlsoStrokes);
		SetPumpPanel2.add(PerMinute);
		
		JPanel NoticePanel = new JPanel();
		NoticePanel.setLayout(new BoxLayout(NoticePanel, BoxLayout.X_AXIS));
		NoticePanel.setOpaque(false);
		Notice.setText("     NOTE: These values are estimates only.     ");
		Notice.setOpaque(false);
		Notice.setForeground(LtRed);
		NoticePanel.add(Notice);
		
		JPanel Notice2Panel = new JPanel();
		Notice2Panel.setLayout(new BoxLayout(Notice2Panel, BoxLayout.X_AXIS));
		Notice2Panel.setOpaque(false);
		Notice2.setText("Use of these numbers in a critical application could result in over/under pumping situations.");
		Notice2.setOpaque(false);
		Notice2.setForeground(LtRed);
		Notice2Panel.add(Notice2);
		
		add(PumpChoicePanel, BorderLayout.PAGE_START);
		add(StrokePanel);
		add(AmtDayPanel);
		add(SetPumpPanel);
		add(SetPumpPanel2);
		add(NoticePanel);
		add(Notice2Panel);
		setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
	}
	
	public void actionPerformed(ActionEvent e) {
		float temp = 0, temp1 = 0;
		int temp2 = 0, temp3 = 0;
		if ("B130Enabled".equals(e.getActionCommand())) {
			OZperPulse.setText("0.021333");
			OZLabel.setSelectedIndex(0);
			MaxStroke.setText("125");
			OZperPulse.setVisible(false);
			OZLabel.setVisible(false);
			MaxStroke.setEditable(false);
		}
		if ("B217Enabled".equals(e.getActionCommand())) {
			OZperPulse.setText("0.01208889");
			OZLabel.setSelectedIndex(0);
			MaxStroke.setText("125");
			OZperPulse.setVisible(false);
			OZLabel.setVisible(false);
			MaxStroke.setEditable(false);
		}
		if ("B115Enabled".equals(e.getActionCommand())) {
			OZperPulse.setText("0.0106667");
			OZLabel.setSelectedIndex(0);
			MaxStroke.setText("125");
			OZperPulse.setVisible(false);
			OZLabel.setVisible(false);
			MaxStroke.setEditable(false);
		}
		if ("B155Enabled".equals(e.getActionCommand())) {
			OZperPulse.setText("0.03055556");
			OZLabel.setSelectedIndex(0);
			MaxStroke.setText("160");
			OZperPulse.setVisible(false);
			OZLabel.setVisible(false);
			MaxStroke.setEditable(false);
		} 
		if ("OtherEnabled".equals(e.getActionCommand())) {
			OZperPulse.setText("0");
			OZperPulse.setVisible(true);
			OZLabel.setVisible(true);
			MaxStroke.setText("0");
			MaxStroke.setEditable(true);
		}
		PumpStrokes.setDisabledTextColor(Blue);
		AlsoStrokes.setDisabledTextColor(Blue);
		temp = Float.parseFloat(AmtPerDay.getText());
		temp1 = Float.parseFloat(OZperPulse.getText());
		if ((OZLabel.getSelectedIndex() == 0) && (AmtPer.getSelectedIndex() == 1)) {
			temp1 *= 29.5735296;		// Convert to mL
		}
		if ((OZLabel.getSelectedIndex() == 1) && (AmtPer.getSelectedIndex() == 0)) {
			temp1 /= 29.5735296;		// Convert to oz
		}
		temp2 = (int)((temp / temp1) / 24);
		temp3 = (int)((temp / temp1) / 24 / 60);
		PumpStrokes.setText(Integer.toString(temp2));
		AlsoStrokes.setText(Integer.toString(temp3));
		if (temp2 > Integer.parseInt(MaxStroke.getText())) {
			PumpStrokes.setText("MAX Exceeded");
			PumpStrokes.setDisabledTextColor(Red);
		}
		if (temp3 > Integer.parseInt(MaxStroke.getText())) {
			AlsoStrokes.setText("MAX Exceeded");
			AlsoStrokes.setDisabledTextColor(Red);
		}
		repaint();
	}
	
	public static void createAndShowGUI() {
		// get screen size & middle of screen position
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int windowX = Math.max(0, (screenSize.width - 600) / 2);
		int windowY = Math.max(0, (screenSize.height - 225) / 2);
		
		// Create and setup the window
		JFrame.setDefaultLookAndFeelDecorated(false);
		JFrame frame = new JFrame("Pump Output Calculator");
		
		// Create and setup the content pane
		JComponent foregroundPanel = new PumpOutputCalc();
		foregroundPanel.setOpaque(true);

		frame.setContentPane(GetFile.wrapInBackgroundImage(foregroundPanel, 
				(GetFile.createImageIcon("bg.jpg"))));
		frame.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) { System.exit(0); }
		});
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		// Display the window
		frame.pack();
		frame.setIconImage(GetFile.getIconImage("Adv-Icon.gif"));
		frame.setBackground(Blue);
		frame.setResizable(false);
		frame.setSize(600, 225);
		frame.setLocation(windowX, windowY);
		frame.setVisible(true);
	}
	
	public static void main(String[] args) {
		// Schedule a job for the event-dispatching thread:
		// creating and showing this application's GUI
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}