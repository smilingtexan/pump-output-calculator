# Pump Output Calculator
## by: Timothy Carter (aka: SmilingTexan)

This is a project I did when I was at [Advantage Controls](http://www.advantagecontrols.com). It is designed to calculate the output of their pumps.

It was accurate as of 2013, but as I no longer work with them, it may not be anymore. Use at your own risk.

The code is a beginner's guide to Java: in fact is from a beginner's look at Java. Since this is one of the first programs I ever developed when I started learning Java.

It's posted here as archive, as well as to help future developers to learn the Java language.
